If using `conda`, you can get this to work as follows:

```
git clone --recurse-submodules git@gitlab.com:boratko/run_boxmodel.git
cd run_boxmodel
conda env create --name run_boxmodel --file conda.yml
pip install -e boxes
pip install -e learner
```

To train boxes you can then run
```
./demo_mammal.py
```

Also check out the `notebooks/Demo (Mammal).ipynb` file.
